Tips examens
-------------
-	Flexbox: zeer belangrijk
	Guide to flexbox!

-	Display properties
	display: none <-> visibility: hidden

-	position: 
	position: sticky

-	float: alleen als je een image rondt tekst wilt verwerken

- 	media queries

-	Tabellen: geen classes of ids: vb. div > p, nth:child
	Tabellen: rijen kunnen samenvoegen
	Tabellen: borders
	Tabellen: breedte cellen
	Tabellen: th en td!

-	Formulieren: mailto: onderwerp toevoegen
	Formulieren: input types
	Formulieren: input attributes: niet multiple en pattern kennen 
	Formulieren: form elements 																																											