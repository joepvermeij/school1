4.4.1

6. SELECT * from medewerkers where maandsal > 5000 and functie != 'MANAGER';

7. select * from medewerkers where maandsal < 2500;

8. select naam from afdelingen where hoofd = 7698;

9. select distinct functie from medewerkers;

10. select MNR, naam from medewerkers where functie = 'TRAINER';

11. select naam, functie, maandsal from medewerkers where afd=30 and maandsal>2500; `

12. select locatie from afdelingen where hoofd = 7566;

13. select naam from medewerkers where afd in (10,20);

14. select naam, (maandsal*12) + NVL(comm,0) as jaarsalaris from medewerkers;

15. select * from medewerkers where naam like '%N';

16. select naam from medewerkers where naam like '%OO%';

17. select naam, maandsal, maandsal*1.1 from medewerkers;

18. select naam, maandsal from medewerkers order by maandsal desc;

19. select naam, maandsal from medewerkers 
where (maandsal <=4000 and maandsal >=2000) and (maandsal !=3850 and maandsal != 2600)
order by maandsal desc;

20. select naam, maandsal from medewerkers order by functie, naam;

21. select naam, voorn, (maandsal*12) + NVL(comm,0) from medewerkers;

case x
	when 0 then 10
	when 1 then 20
	else 30
end

case
	when x = 0 then 10
	when x = 1 then 20
	else 30

4.4.2

1.
select voorn, naam, maandsal
from medewerkers
where maandsal > ( select maandsal from medewerkers where naam = 'CLERCKX');
2.
select voorn, naam
from medewerkers
where afd = ( select afd from medewerkers where naam = 'ALLARD');
3.
select voorn, naam
from medewerkers
where MNR in ( select cursist from inschrijvingen where cursus is not NULL);
4.
select naam
from medewerkers
where gbdatum > (select gbdatum from medewerkers where naam = 'JACOBS' and voorn like 'E%');
5.
select distinct cursist
from inschrijvingen
where cursus in
(select cursus from uitvoeringen where locatie = 'MAASEIK')
and begindatum in (select begindatum from uitvoeringen where locatie = 'MAASEIK');
6.
select omschrijving, Type
from cursussen
where CODE in 
(select cursus from inschrijvingen where cursist in (select MNR from medewerkers where Functie = 'VERKOPER'));
7.
Select naam, voorn
from medewerkers
where MNR in
(select cursist from inschrijvingen where cursus in 
(select cursus from uitvoeringen where Docent=7369) and 
begindatum in(select begindatum from uitvoeringen where Docent=7369));
8.
select omschrijving
from cursussen 
where code in(
select cursus from uitvoeringen where Docent is NULL);
9.
Select naam, voorn
from medewerkers
where afd in
(select anr from afdelingen where naam ='VERKOOP')
and comm is NULL;
10.
Select naam, voorn
from medewerkers
where MNR in
(select cursist from inschrijvingen where cursus in 
(select cursus from uitvoeringen where Docent is NULL) and 
begindatum in(select begindatum from uitvoeringen where Docent is NULL));

4.4.3

13)
select
naam "Naam",
decode(functie,'TRAINER', 'LERAAR', 'DIRECTEUR', 'HOOFD', 'MEDEWERKER')
from medewerkers;

select naam "Naam",
case functie
when 'TRAINER' then 'LERAAR'
when 'DIRECTEUR' then 'HOOFD'
else 'MEDEWERKER'
END
from medewerkers
order by case functie;

14)
col "Grootste waarde" format a30;

select
naam "Naam",
case greatest(mnr - 2000, power(ascii(naam), 2))
when (mnr-2000) then 'mnr - 2000'
else 'letterwaarde'
end "grootste waarde"
from medewerkers;

15)
select
naam,
functie,
case functie
when 'TRAINER' then to_char(maandsal)
when 'VERKOPER' then to_char(comm)
else 'onbelangrijk'
end overzicht
from medewerkers;

16)
select
naam "Naam",
LOWER(TO_CHAR(gbdatum, 'DAY')) "weekdag",
TO_CHAR(EXTRACT(day from gbdatum), '09') "Dag",
lower(to_char(gbdatum, 'MONTH')) "Maand",
extract(year from gbdatum) "Jaar"
from medewerkers
where functie = 'MANAGER';

17)
select trunc(sysdate - date '1995-06-05') "dagen oud" from dual;

18)
alter session set nls_language=English; 

select naam, to_char(gbdatum, '"The" Ddthsp "of" fmMonth Yyyysp') "GEBOORTEDATUM"
from medewerkers
where functie = 'TRAINER';
alter session set nls_language=Dutch;

19)
select naam "naam",
to_char(gbdatum, 'W') "weeknummer",
to_char(gbdatum, 'Q') "Kwartaal",
to_char(gbdatum, 'J') "Juliaans dagnummer"
from medewerkers
where naam in ('WOUTERS', 'SWINNEN');

20)
select voorn, naam, extract(hour from cast(gbdatum as timestamp)) uur geboorte
from medewerkers;

21)
select voorn, naam, gbdatum "Geboortedatum", add_months(gbdatum, 6) "6 maanden later"
from medewerkers;


5.7.2

11)
select naam
from medewerkers m ,
where m.maandsal > 
		(select avg(mm.maandsal)
		from medewerkers mm	
		where  mm.afd = m.afd
		group by mm.afd);

12)
select m.naam, count(i.cursus) aantal
from medewerkers m
join inschrijving i
on m.mnr =i.cursist
group by m.naam
order by aantal;

13)
Wat is het gemiddelde salaris van de medewerkers 
die in dezelfde afdeling werken als 
'Den Ruyter'?
 
select avg(maandsal)
from medewerkers
where afd in 
    (select afd
    from medewerkers
    where naam = 'DEN RUYTER');
 
select avg(m.maandsal)
from medewerkers m
join medewerkers mm
on mm.naam = 'DEN RUYTER'
where m.afd = mm.afd;

15)
select m.naam, m.maandsal
from medewerkers m
where 3 >= 
    (select count(n.mnr)
    from medewerkers n
     where n.maandsal >= m.maandsal)
order by m.maandsal desc;
 
select naam
from medewerkers
order by maandsal desc
fetch 3 next rows only;

17)
select m.naam, m.maandsal
from medewerkers m
where 5 >=
    (select count(n.mnr)
    from medewerkers n
     where n.maandsal < m.maandsal)
order by m.maandsal desc;

19)
select max(avg(maandsal))
from medewerkers
group by afd;
 

select max(t.gemiddelde)
from
    (select avg(maandsal) gemiddelde
    from medewerkers
    group by afd) t;
21)
select max(begindatum)
from uitvoeringen;


5.7.3
1)
select m.naam from medewerkers m;
union
select a.naam from afdelingen a;

5)
select dstinct c.omschrijving, i.begindatum
from cursussen c
join inschrijvingen i
on c.code = i.cursus
where i.cursist = 7844;