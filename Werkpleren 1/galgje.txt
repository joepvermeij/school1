if (guess == antwoord)
            {
                Resultaat.Text = $"Hoera!!!\n Je hebt \n \"{antwoord}\" \n correct geraden!";
            }
            else if (guess.Length != 1)
            {
                levens--;
                Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}";
            }
            else if (guess.Contains(antwoord))
            {
                juisteLetters = juisteLetters + guess;
                Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}";
            }
            else
            {
                levens--;
                fouteLetters = fouteLetters + guess;
                Resultaat.Text = $"{levens} Levens\nJuiste Letters: {juisteLetters}\nFoute Letters: {fouteLetters}";
            }
            if (levens== 0)
            {
                Resultaat.Text = $"Je hebt het geheim niet op tijd geraden. \nJe bent opgehangen";
                Raad.Visibility = Visibility.Hidden;
            }
            else
            {
            }