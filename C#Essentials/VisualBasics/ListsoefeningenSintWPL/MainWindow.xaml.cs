﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListsoefeningenSintWPL
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void RegistreerButton_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem niewListBoxItem = new ListBoxItem();
            niewListBoxItem.Content = InputTextBox.Text;
            if (StoutOfFlinkComboBox.SelectedIndex == 0)
            {
                StoutListBox.Items.Add(niewListBoxItem);
            }
            else if (StoutOfFlinkComboBox.SelectedIndex == 1)
            {
                FlinkListBox.Items.Add(niewListBoxItem);
            }
            else
            {
            }
            InputTextBox.Text = "";
        }

        private void VerwijderButton_Click(object sender, RoutedEventArgs e)
        {
            if (FlinkListBox.SelectedItem != null)
            {
                FlinkListBox.Items.Remove(FlinkListBox.SelectedItem);
            }
            else if (StoutListBox.SelectedItem != null)
            {
                StoutListBox.Items.Remove(StoutListBox.SelectedItem);
            }
            {

            }
        }

        private void LinksButton_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem niewListBoxItem = new ListBoxItem();
            niewListBoxItem.Content = StoutListBox.SelectedItem;
            FlinkListBox.Items.Add(niewListBoxItem);
            StoutListBox.Items.Remove(StoutListBox.SelectedItem);
        }

        private void RechtsButton_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem niewListBoxItem = new ListBoxItem();
            niewListBoxItem.Content = FlinkListBox.SelectedItem;
            StoutListBox.Items.Add(niewListBoxItem);
            FlinkListBox.Items.Remove(FlinkListBox.SelectedItem);
        }
    }
}
