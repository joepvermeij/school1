﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfAppDemoDoc
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
         
        private void Test()
        {
            // input converteren

            // do stuff

            // output tonen in disabled textbox

            bool isGoedkoper = IsKlantGeldigVoorKorting("16523523523523", true, 20);
        }

        /// <summary>
        /// Neemt het id van het klantensysteem en controleert voor geldigheid. Indien niet geldig, return false.
        /// Als geldig id, is VIP en meer dan 5 jaar klant, dan geeft hij korting.
        /// </summary>
        /// <param name="KlantId">16 bit string</param>
        /// <param name="isVIP">Heeft de klant een premium account</param>
        /// <param name="aantalJarenKlant">Het aantal jaren dat de klant trouw is aan de winkel</param>
        /// <returns>
        /// Returns false als ongeldig account of klant krijgt geen korting
        /// Returns true als klant korting krijgt.
        /// </returns>
        private bool IsKlantGeldigVoorKorting(string klantId, bool isVIP, int aantalJarenKlant)
        {
            // doe heel veel werk
            string id = klantId;
            return false;
        }
    }
}
