﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Verwachte_Lengte
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            double GetalEiwit = Convert.ToDouble(Eiwit.Text);
            double GetalCaloriën = Convert.ToDouble(Caloriën.Text);
            double GetalEiwit100 = (GetalEiwit / GetalCaloriën) * 100;
            double GetalEiwitPercent = (GetalEiwit100*20 / 160*100 );
            EiwitPercent.Text = Convert.ToString(GetalEiwitPercent);
            Eiwit100.Text = Convert.ToString(GetalEiwit100);
        }
    }
}
