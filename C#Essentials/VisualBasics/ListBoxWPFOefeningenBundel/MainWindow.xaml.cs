﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListBoxWPFOefeningenBundel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void VoegToeButton_CLick(object sender, RoutedEventArgs e)
        {
            ListBoxItem nieuwListBoxItem = new ListBoxItem();
            nieuwListBoxItem.Content = VoegToeTextBox.Text;
            MijnListBox.Items.Add(nieuwListBoxItem);
            VoegToeTextBox.Clear();
        }

        private void VervangButton_Click(object sender, RoutedEventArgs e)
        {
            //if(MijnListBox.SelectedIndex != -1)
            //{
            //    ListBoxItem nieuwListBoxItem = new ListBoxItem();
            //    nieuwListBoxItem.Content = VervangTextBox.Text;
            //    MijnListBox.Items.Insert(MijnListBox.SelectedIndex, nieuwListBoxItem);
            //    MijnListBox.Items.Remove(MijnListBox.SelectedItem);
            //    VervangTextBox.Clear();
            //}

            if (MijnListBox.SelectedIndex != -1)
            {
                ListBoxItem geselecteerdItem = (ListBoxItem)MijnListBox.SelectedItem;
                geselecteerdItem.Content = VervangTextBox.Text;
                VervangTextBox.Clear();
            }
        }

        private void VerwijderButton(object sender, RoutedEventArgs e)
        {
            if (MijnListBox.SelectedIndex != -1)
            {
                MijnListBox.Items.RemoveAt(MijnListBox.SelectedIndex);
            }
        }

        private void WissenButton(object sender, RoutedEventArgs e)
        {
            MijnListBox.Items.Clear();
        }

        private void ZoekenButton_Click(object sender, RoutedEventArgs e)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine("exacte matches: ");
            for (int i = 0; i < MijnListBox.Items.Count; i++)
            {
                ListBoxItem item = (ListBoxItem) MijnListBox.Items[i];

                if (ZoekTextBox.Text.Equals(item.Content.ToString()))
                {
                    stringBuilder.AppendLine(item.Content.ToString());
                }
                
            }

            stringBuilder.AppendLine("Gevonden resultaten: ");
            foreach (ListBoxItem items in MijnListBox.Items)
            {
                if (items.Content.ToString().Contains(ZoekTextBox.Text))
                {
                    stringBuilder.AppendLine(items.Content.ToString());
                }
            }
            ZoekResultatenTextBox.Text = stringBuilder.ToString();
        }
    }
}
