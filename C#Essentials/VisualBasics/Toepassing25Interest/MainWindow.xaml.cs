﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmInterest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Bereken_Click(object sender, RoutedEventArgs e)
        {
            double beginKapitaal = Convert.ToDouble(BeginKapitaalTextBox.Text);
            double gewenstEindKapital = Convert.ToDouble(GewenstEindkapitaalTextBox.Text);
            double intrestVoet = Convert.ToDouble(IntrestVoetTextBox.Text);
            double nieuwKapitaal = beginKapitaal;
            double jaren = 0;
            StringBuilder resultaatTekstBouwer = new StringBuilder();
            while (gewenstEindKapital > nieuwKapitaal)
            {
                jaren++;
                nieuwKapitaal += nieuwKapitaal * (intrestVoet/100);
                resultaatTekstBouwer.AppendLine($"Waarde na {jaren} jaren: {string.Format("{0:0.00}", nieuwKapitaal)} euro");
            }
            ResultaatTextBlock.Text = resultaatTekstBouwer.ToString();
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            BeginKapitaalTextBox.Text = "";
            ResultaatTextBlock.Text = "";
            IntrestVoetTextBox.Text = "";
            GewenstEindkapitaalTextBox.Text = "";
            BeginKapitaalTextBox.Focus();
        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Opmaak_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemComma || e.Key == Key.Back || (e.Key >= Key.D0 && e.Key <= Key.D9) || (e.Key >= Key.NumPad0 && e.Key <= Key.NumPad9))
            {
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
