﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Voorbeeldexamen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<string> namen = new List<string>() { "Sander", "Quirino", "Thomas",
        "Cédric", "Jason", "Domenico", "Rickert", "Klaas", "Tom", "Stephan", "Alexander",
        "Yannick", "Robin", "Dave", "Lynn", "Arno", "Niels", "Maxiem", "Matthijs", "Kobe",
        "Michaël", "Bram", "Achraf", "Raf", "Sven", "Gerben", "Kevin", "Cem", "Steff", "Steven",
        "Rani", "Djordy", "Nick", "Mikail", "Konstantin", "Asad", "Viktor", "Antonio", "Senne",
        "Benjamin", "Stef", "Abdul", "Christiaan", "Abdurrahman", "Jurgen", "Kevin", "Silvio",
        "Nathan", "Stijn", "Bart", "Frank", "Steven", "Matty", "Arend", "Simon", "Ziggy",
        "Pascal", "Michaël", "Danny", "Robby", "Johan", "Vincent", "Wim", "Tuba", "Kristof",
        "Kenneth" };
        private string[,] lidgeldPerCat = { { "Preminiem", "150" }, { "Miniem", "150" }, {
        "Junior", "170" }, { "Kadet", "170" },{ "Senior", "200" } };
        private TextBox[] prognoseVakken = new TextBox[6];
        DispatcherTimer Klok;
        public MainWindow()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Vult De ComboBox Met Alle namen van de list
        /// </summary>
        private void VulComboBox()
        {
            foreach  (string item in namen)
            {
                ComboBoxItem naam= new ComboBoxItem();
                naam.Content = item;
                NamenComboBox.Items.Add(naam);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            VulComboBox();
            LaadTextBoxen();
            KlokAanmaken();
        }
        /// <summary>
        /// Maakt de klok aan die onderaan het scherm wordt getoond.
        /// </summary>
        private void KlokAanmaken()
        {
            Klok = new DispatcherTimer();
            Klok.Tick += new EventHandler(DispatcherTimer_Tick);
            Klok.Interval = new TimeSpan(0, 0, 1);
            Klok.Start();
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            Tijdlbl.Content = DateTime.Now.ToLongDateString() + "  " + DateTime.Now.ToLongTimeString();
        }
        /// <summary>
        /// Laad de Array van TextBoxen met de opmaak van de textboxen in xaml.
        /// </summary>
        private void LaadTextBoxen()
        {
            prognoseVakken[0] = NuTextBlock;
            prognoseVakken[1] = Na1JaarTextBlock;
            prognoseVakken[2] = Na2JaarTextBlock;
            prognoseVakken[3] = Na3JaarTextBlock;
            prognoseVakken[4] = Na4JaarTextBlock;
            prognoseVakken[5] = Na5JaarTextBlock;
        }
        /// <summary>
        /// Zet alles terug naar startwaarde.
        /// </summary>
        private void clear()
        {
            NamenComboBox.SelectedIndex = -1;
            NamenComboBox.Focus();
            CompetitieLidCheckBox.IsChecked = false;
            NieuwLidCheckBox.IsChecked = true;
            RangGezinTextBox.Text = "";
            TijdTextBox.Text = "";
            JuniorRadioButton.IsChecked = true;
            ResultaatTextBox.Text = "";
            NuTextBlock.Text = "Prognose";
            Na1JaarTextBlock.Text = "Prognose";
            Na2JaarTextBlock.Text = "Prognose";
            Na3JaarTextBlock.Text = "Prognose";
            Na4JaarTextBlock.Text = "Prognose";
            Na5JaarTextBlock.Text = "Prognose";
        }
        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            clear();
        }
        private void Berekenen(object sender, RoutedEventArgs e)
        {
            try
            {
                BerekenTijdPrognose();
                double basisBedrag = Categorie();
                double teBetalen = basisBedrag + CompetetieLid();
                teBetalen = teBetalen - (teBetalen * RangGezinKorting());
                ResultaatTextBox.Text = $"Inschrijvingsgeld voor {namen[NamenComboBox.SelectedIndex]}\n\nBasisbedrag voor {namen[NamenComboBox.SelectedIndex]}: {basisBedrag} \nTe Betalen: {teBetalen}";
            }
            catch (Exception)
            {
                MessageBox.Show("Geef Een Natuurlijk Getal in of selecteer uw naam.");
            }
        }
        /// <summary>
        /// Geeft De Toeslag terug voor competitieleden
        /// </summary>
        private int CompetetieLid()
        {
            if (CompetitieLidCheckBox.IsChecked == true)
            {
                return 50;
            }
            else
            {
                return 0;
            }
        }
        /// <summary>
        /// Geeft De Korting Terug op basis Van hoeveel gezinsleden er al zijn
        /// </summary>
        private double RangGezinKorting()
        {
            int rangGezin = Convert.ToInt32(RangGezinTextBox.Text);
            return (rangGezin-1) * 0.05;
        }
        /// <summary>
        /// Geeft de basisprijs terug op basis van welke categorie geselecteerd is.
        /// </summary>
        /// <returns></returns>
        private int Categorie()
        {
            if (PreminiemRadioButton.IsChecked==true)
            {
                return 150;
            }
            else if (MiniemRadioButton.IsChecked == true)
            {
                return 150;
            }
            else if (CadetRadioButton.IsChecked == true)
            {
                return 170;
            }
            else if (SeniorRadioButton.IsChecked == true)
            {
                return 200;
            }
            else
            {
                return 170;
            }
        }
        /// <summary>
        /// Berekent en update de layout van de Tijdprognose in seconden
        /// </summary>
        private void BerekenTijdPrognose()
        {
            double prognoseVakkenWidth = 400;
            double prognoseVakkenText = Convert.ToDouble(TijdTextBox.Text);
            for (int i = 0; i < 6; i++)
            {
                prognoseVakkenWidth -= (prognoseVakkenWidth * (0.05 - (i * .005)));
                prognoseVakkenText = prognoseVakkenText - (prognoseVakkenText * (0.05 - (i * .005)));
                prognoseVakken[i].Width = prognoseVakkenWidth;
                prognoseVakken[i].Text = string.Format("{0:0.00}", prognoseVakkenText);
            }
            NuTextBlock = prognoseVakken[0];
            Na1JaarTextBlock = prognoseVakken[1];
            Na2JaarTextBlock = prognoseVakken[2];
            Na3JaarTextBlock = prognoseVakken[3];
            Na4JaarTextBlock = prognoseVakken[4];
            Na5JaarTextBlock = prognoseVakken[5];
        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
