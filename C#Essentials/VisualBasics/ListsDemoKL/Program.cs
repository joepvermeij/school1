﻿using System;
using System.Collections.Generic;

namespace ListsDemoKL
{
    class Program
    {
        static void Main(string[] args)
        {
            #region startDemo
            //// declaratie
            //List<int> getallen;
            //// initialisatie
            //getallen = new List<int>();

            //// initialiseren met elementen
            //int[] reeks = new int[] { 1, 2, 3 };
            //List<int> listVanGetallen = new List<int>() { 1, 2, 3 };

            //// initialiseren obv een array
            //List<int> listVanArray = new List<int>(reeks);

            //Console.WriteLine(listVanArray[0]);
            //listVanArray[0] = 9001;
            //Console.WriteLine(listVanArray[0]);

            //List<string> eten = new List<string>() { "blik bruine bonen" };
            //// add
            //eten.Add("zak rijst");
            //eten.Add("Pepers");
            //eten.Add("curry");
            //Console.WriteLine("Elementen in de list:");
            //// count
            //for (int i = 0; i < eten.Count; i++)
            //{
            //    Console.WriteLine(eten[i]);
            //}
            //Console.WriteLine("Aantal elementen in de list " + eten.Count);
            //// clear
            //eten.Clear();
            //Console.WriteLine("Aantal elementen in de list " + eten.Count);

            //// Contains
            //eten.Add("fles limonade");
            //eten.Add("nootjes");
            //if (eten.Contains("fles limonade"))
            //{
            //    Console.WriteLine("Del fles  limonade is in de list");
            //}
            //// insert
            //for (int i = 0; i < eten.Count; i++)
            //{
            //    Console.WriteLine($"Het element '{eten[i]}' zit op index '{i}'");
            //}
            //eten.Insert(0, "blik bruine bonen");
            //for (int i = 0; i < eten.Count; i++)
            //{
            //    Console.WriteLine($"Het element '{eten[i]}' zit op index '{i}'");
            //}
            //eten.Add("blik bruine bonen");
            //eten.Add("blik bruine bonen");

            //// Remove
            //Console.WriteLine("Remove:");
            //eten.Remove("blik bruine bonen");
            //for (int i = 0; i < eten.Count; i++)
            //{
            //    Console.WriteLine($"Het element '{eten[i]}' zit op index '{i}'");
            //}
            //// RemoveAt
            //eten.RemoveAt(3);
            //// IndexOf
            //int indexVanElementBonen = eten.IndexOf("blik bruine bonen");
            //Console.WriteLine(indexVanElementBonen);

            //Console.ReadKey();

            //List<string> series = new List<string>();
            //string jaNee;
            //do
            //{
            //    Console.WriteLine("Geef u favoriete series:");
            //    series.Add(Console.ReadLine());
            //    Console.WriteLine("Wilt u nog een serie toevoegen?(ja/nee)");
            //    jaNee = Console.ReadLine();
            //} while (jaNee == "ja");
            //Console.WriteLine(string.Join(", ", series));
            //Console.ReadKey();
            #endregion

            // foreach
            List<int> getallen = new List<int>() { 1, 2, 3, 4, 5 };
            foreach (int element in getallen)
            {
                Console.WriteLine(element);
            }
            List<string> woorden = new List<string>() { "hond", "tafel", "raam" };
            foreach (string woord in woorden)
            {
                Console.WriteLine(woord);
            }
        }
    }
}
