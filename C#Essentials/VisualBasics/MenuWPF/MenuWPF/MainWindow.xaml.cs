﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MenuWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void AfsluitenMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Fill(object sender, RoutedEventArgs e)
        {
            AchtergrondImage.Stretch = Stretch.Fill;
        }

        private void Uniform(object sender, RoutedEventArgs e)
        {
            AchtergrondImage.Stretch = Stretch.Uniform;
        }

        private void None(object sender, RoutedEventArgs e)
        {
            AchtergrondImage.Stretch = Stretch.None;
        }

        private void UniformtoFill(object sender, RoutedEventArgs e)
        {
            AchtergrondImage.Stretch = Stretch.UniformToFill;
        }
    }
}
