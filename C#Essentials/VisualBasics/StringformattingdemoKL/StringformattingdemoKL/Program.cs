﻿using System;

namespace StringformattingdemoKL
{
    class Program
    {
        static void Main(string[] args)
        {
           /* #region startdemo
            Console.WriteLine("geef een getal in");
            string input = Console.ReadLine();
            int getal;
            bool isconverteerbaar = int.TryParse(input, out getal);
            if(isconverteerbaar)
            {
                int echtewaarde = Convert.ToInt32(input);
            }
            Console.WriteLine(getal);
            # endregion startdemo*/
            /**
             * string formatting
             * 
             * variabelen van het programma in tekst injecteren
             * 
             */
            int getal1 = 1;
            int getal2 = 10;
            int getal3 = 100;
            int getal4 = 1000;


            // resultaat = "1 + 10 = 11"

            string result = getal1 + " + " + getal2 + " + " + getal3 + " + " + getal4;

            string beterresultaat = $"{getal1} + {getal2} + {getal3} + {getal4}";
            Console.WriteLine(result);
            Console.WriteLine(beterresultaat);

            Console.WriteLine("{1}: Dit is een test met getal één: {0}", getal1, getal2);

            string test="\t\n\r\\";

            string testzonderescape = @"Dit
is een tekst van meerdere lijnen.
Hier worden geen tabs gebruikt om de
tekst uit te lijnen.";

            Console.WriteLine(testzonderescape);

        }
    }
}
