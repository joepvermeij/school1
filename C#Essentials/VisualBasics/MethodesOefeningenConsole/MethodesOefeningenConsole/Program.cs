﻿using System;

namespace MethodesOefeningenConsole
{
    class Program
    {
        static int a;
        static int b;
        static void Main(string[] args)
        {
            //oef 1
            //double a = Convert.ToInt32(Console.ReadLine());
            //double b = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine($"getal 1 = {a} en getal 2 = {b}");
            //Wissel(ref a, ref b);
            //Console.WriteLine($"getal 1 = {a} en getal 2 = {b}");

            //oef 5
            // Te gebruiken
            // private vs public
            // aantal argumenten
            // static vs non static
            // Naam(PascalCasing)
            // Method body
            // return type (void, int, string)
            //double resultaat = Power(2, 5);
            //Console.WriteLine($"de berekening van het grondtal 2 met als exponent 5 = {resultaat}");

            // oef 7

            double a = Convert.ToDouble(Console.ReadLine());
            Dubbel(ref a);
            Console.WriteLine(a);
        }

        // oef 1
        //private static void Wissel (ref double getal1, ref double getal2)
        //{
        //    double wisselSwitch = getal1;
        //    getal1 = getal2;
        //    getal2 = wisselSwitch;
        //}

        //oef 5
        //private static double Power(double getal1, double getal2)
        //{
        //    double uitkomst = 1;
        //    for (int i = 0; i < getal2; i++)
        //    {
        //        uitkomst = uitkomst * getal1;
        //    }
        //    return uitkomst;
        //}

        // oef 7
        private static void Dubbel (ref double getal1)
        {
            getal1 = getal1 * 2; 
        }

        // oef 10
        private static long Faculteit (long n)
        {
            
            if (n <= 1)
            {
                return 1;
            }
            else
            {
                return n * Faculteit(n - 1);
            }
        }
    }
}
