﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DictionaryDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            int[] drieGetallen = new int[3] { 1, 2, 3 };
            int[] reeksVanGetallen = new int[10];
            reeksVanGetallen[0] = 10;
            reeksVanGetallen[9] = 12;

            // declaratie
            Dictionary<string, double> restaurantMetScores;
            // initialisatie
            

            int[,] matrix = new int[3, 3]
            {
                {1,2,3 },
                {4,5,6 },
                {7,8,9 }
            };

            StringBuilder stringBuilder = new StringBuilder();
            restaurantMetScores = new Dictionary<string, double>()
            {
                {"Jip's place", 10 },
                {"The Century", 8 },
                {"Centrum", 6.12 }
            };
            Dictionary<int, string> dict1 = new Dictionary<int, string>();
            dict1[0] = "test";
            dict1[10000] = "groot";
            dict1[-200] = "test";
            dict1[-200] = "negatief";
            dict1[10000] = dict1[0];
            //Testen van Count
            stringBuilder.AppendLine($"Er zijn {restaurantMetScores.Count}" + $" aantal paren in de dictionary");
            restaurantMetScores["Pizza Hut"] = 7.5;
            stringBuilder.AppendLine($"Er zijn {restaurantMetScores.Count}" + $" aantal paren in de dictionary");
            // .Add testen
            restaurantMetScores.Add("KFC", 6.66);
            // Testen van Clear met Count
            //restaurantMetScores.Clear();
            stringBuilder.AppendLine($"Al de sleutels:");
            foreach (string key in restaurantMetScores.Keys)
            {
                stringBuilder.AppendLine($"{ key}: {restaurantMetScores[key]}") ;
            }

            if (restaurantMetScores.ContainsKey("KFC"))
            {
                stringBuilder.AppendLine("KFC is opgenomen in de scores voor restaurant");
            }

            if(!restaurantMetScores.ContainsKey("Martinell"))
            {
                stringBuilder.AppendLine("De Martinell is niet beoordeeld");
            }

            if (restaurantMetScores.ContainsValue(10))
            {
                stringBuilder.AppendLine("we gaan naar het perfecte restaurant");
            }

            OutputTextBox.Text = stringBuilder.ToString();

        }
    }
}
