﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cursusgeld
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
       

        private void Testnumeriekbutton_Click(object sender, RoutedEventArgs e)
        {
            // input: jaartal opvragen
            string jaartalintekst = Jaar.Text;
            // jaartal proberen te converteren (try parse)
            int jaartal;
            bool isconverteerbaar = int.TryParse(jaartalintekst, out jaartal);

            if(isconverteerbaar)
            {
                IsnumeriekTextBlock.Text = "Deze inhoud is numeriek";
            }
            else
            {
                IsnumeriekTextBlock.Text = "Deze inhoud is niet numeriek";
            }

            // als converteerbaar
            // dit is een getal
            // anders
            // output dit is geen getal


        }
        private void Jaar_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsnumeriekTextBlock != null)
            {
            IsnumeriekTextBlock.Text = "";

            }
        }

        private void Bereken(object sender, RoutedEventArgs e)
        {
           string jaartalintekst = Jaar.Text;
           string studiepuntenintekst = Studiepunten.Text;
           string inschrijvingsgeldintekst = inschrijvingsgeld.Text;
            double inschrijf;
            bool whatever = double.TryParse(inschrijvingsgeldintekst, out inschrijf);
           double jaartal;
           double studiepuntengetal;
           bool isconverteerbaar = double.TryParse(jaartalintekst, out jaartal);
           bool isconverteerbaar2 = double.TryParse(studiepuntenintekst, out studiepuntengetal);
            if(isconverteerbaar && isconverteerbaar2)
            {
                // het is een getal, we kunnen het schrikkeljaar berekenen
                bool isschrikkeljaar = (jaartal % 4 == 0 && jaartal % 100 != 0) | jaartal % 400 == 0;
                if(isschrikkeljaar)
                {
                    inschrijf = (studiepuntengetal + 8) * 15.65;
                }
                else
                {
                    inschrijf = studiepuntengetal * 15.65;
                }
            }
            else
            {
                // het is geen getal we stoppen
                Close();
            }
     
         }

        
    }
}
