﻿using System;

namespace VeelGebruikteKlassesKLoefeningen
{
    class Program
    {
        static void Main(string[] args)
        {
            //1
            //Console.WriteLine("Getal 1?");
            //int a = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Getal 2?");
            //int b = Convert.ToInt32(Console.ReadLine());
            //Console.WriteLine("Getal 3?");
            //int c = Convert.ToInt32(Console.ReadLine());
            //int d = Math.Max(a, b);
            //Console.WriteLine(Math.Max(d,c));

            //2
            //Random dobbelsteen = new Random();
            //Console.WriteLine(dobbelsteen.Next(0,7));

            //3
            Console.WriteLine("Geef de aantal zijdes van de dobbelsteen.");
            int a = Convert.ToInt32(Console.ReadLine()) + 1;
            Random dobbelsteen = new Random();
            Console.WriteLine(dobbelsteen.Next(1,a));
        }
    }
}
