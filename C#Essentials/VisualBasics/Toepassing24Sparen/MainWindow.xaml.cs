﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmSparen
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Berekenen_Click(object sender, RoutedEventArgs e)
        {
            double weekGeld = Convert.ToDouble(WeekGeldTextBox.Text);
            double wekelijkseVerhoging = Convert.ToDouble(WekelijkseVerhogingTextBox.Text);
            double nieuwWeekGeld = wekelijkseVerhoging;
            double huidigSpaarBedrag = 0;
            double gewenstSpaarBedrag = Convert.ToDouble(GewenstSpaarBedragTextBox.Text);
            int week = 0;
            while (huidigSpaarBedrag<gewenstSpaarBedrag)
            {
                week++;
                nieuwWeekGeld += wekelijkseVerhoging;
                huidigSpaarBedrag += nieuwWeekGeld;
            }
            ResultaatTextBlock.Text = $"Spaarbedrag na {week--} weken: {String.Format("{0:0.00}", huidigSpaarBedrag - nieuwWeekGeld)} euro \n\n Extra weekgeld op dat moment: {nieuwWeekGeld - weekGeld} euro \n\n Totaal spaargeld: {String.Format("{0:0.00}", huidigSpaarBedrag)} euro";
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            WeekGeldTextBox.Text = "";
            WekelijkseVerhogingTextBox.Text = "";
            GewenstSpaarBedragTextBox.Text = "";
            ResultaatTextBlock.Text = "";
            WeekGeldTextBox.Focus();
        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
