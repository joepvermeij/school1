﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmWeddeberekening
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Berekenen(object sender, RoutedEventArgs e)
        {
            float aantalgewerkteuren = float.Parse(Aantaluren.Text);
            float uurloon = float.Parse(Uurloon.Text);
            float brutojaarwedde;
            float belasting;
            float nettojaarwedde;

            brutojaarwedde = uurloon * aantalgewerkteuren;
            switch (brutojaarwedde)
            {
                case <= 10000:
                    belasting = 0f;
                    break;
                case <= 15000:
                    belasting = (brutojaarwedde-10000f) * .2f;
                    break;
                case <= 25000:
                    belasting = 5000*.2f + (brutojaarwedde-15000f) * .3f;
                    break;
                case <= 50000:
                    belasting = 5000 * .2f + 10000 * .3f + (brutojaarwedde - 25000f) * .4f;
                    break;
                default:
                    belasting = 5000 * .2f + 10000 * .3f + 25000 * .4f + (brutojaarwedde - 50000f) * .5f;
                    break;
            }
            nettojaarwedde = brutojaarwedde - belasting;
            Resultaat.Text = Convert.ToString(nettojaarwedde);
        }
    }
}
