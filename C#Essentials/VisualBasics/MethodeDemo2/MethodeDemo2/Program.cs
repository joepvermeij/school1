﻿using System;

namespace MethodeDemo2
{
    class Program
    {
        static int heelbelangrijkewaarde = 0;
        public static void Main(string[] args)
        {
            
            Dosomestuff();
            string voornaam = "";
            string achternaam = verzamelnaam(out voornaam);
            Console.WriteLine(voornaam);
            Console.WriteLine(achternaam);

            Math.Abs(2);

            short kleingetal = 1;
            short tweedekleingetal = 3;
            long langgetal = 2, tweedelanggetal = 4;

            short kleinresultaat = Maal(kleingetal, tweedekleingetal);
            long langresultaat = Maal(langgetal, tweedelanggetal);
            Console.WriteLine(kleinresultaat);
            Console.WriteLine(langresultaat);
        }

        private static void Dosomestuff()
        {
            Console.WriteLine(heelbelangrijkewaarde);
        }

        private static string verzamelnaam(out string voornaam)
        {
            // bool isconverteerbaar = int.TryParse("9", out int getal);
            Console.WriteLine("wat is jer achternaam?");
            string achternaam = Console.ReadLine();
            Console.WriteLine("wat is je voornaam?");
            voornaam = Console.ReadLine();

            return achternaam;
        }
        private static short Maal(short a, short b)
        {
            Console.WriteLine("Dit zijn short values");
            return Convert.ToInt16(a * b);
        }
        private static long Maal(long a, long b)
        {
            Console.WriteLine("Dit zijn long values");
            return a * b;
        }
    }
}
