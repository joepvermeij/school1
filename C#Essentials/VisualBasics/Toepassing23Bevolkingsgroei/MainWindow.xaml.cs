﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Bereken_Click(object sender, RoutedEventArgs e)
        {
            double huidigeBevolking = Convert.ToDouble(HuidigeBevolkingTextBox.Text);
            double groeiPercentage = Convert.ToDouble(GroeiPercentageTextBox.Text)/100;
            double nieuweBevolking = huidigeBevolking;
            int jaren = 0;
            if (groeiPercentage > 0)
            {
                do
                {
                    jaren++;
                    nieuweBevolking += nieuweBevolking * groeiPercentage;
                } while (huidigeBevolking * 2 > nieuweBevolking);
                Resultaatlbl.Content = $"Verdubbeling bevolking in {LandTextBox.Text} na {jaren} jaren" +
                    $"\nNieuw bevolkingsaantal op dat moment: {nieuweBevolking}";
            }
            else
            {
                Resultaatlbl.Content = "zonder groeipercentage nooit een verdubbeling van de bevolking";
            }
        }

    private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            Resultaatlbl.Content = "";
            LandTextBox.Text = "";
            GroeiPercentageTextBox.Text = "";
            HuidigeBevolkingTextBox.Text = "";
            LandTextBox.Focus();
        }

        private void Afsluiten_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
