﻿using System;

namespace ConsoleIteratieDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //for (int i = 1; i > -5; i--)
            //{
            //    Console.WriteLine(i);
            //}

            //string input = Console.ReadLine();
            //while (Convert.ToInt32(input) > 0)
            //{
            //    Console.WriteLine("Blijft in de lus");
            //    input = Console.ReadLine();
            //}
            //Console.WriteLine("Voor de do-lus");
            //string input = Console.ReadLine();
            //do
            //{
            //    Console.WriteLine("Zit in de lus");
            //    input = Console.ReadLine();
            //} while (Convert.ToInt32(input)>0);

            //Console.WriteLine("Een vierkant afdrukken:\n"); 
            //for (int i = 0; i < 5; i++) 
            //{ 
            //    for (int j = 0; j < 5; j++) 
            //    { 
            //        Console.Write("* "); 
            //    } 
            //    Console.WriteLine(); 
            //}

            // C# oefeningen

            // 1
            //for (int i = 1; i <= 10; i++)
            //{
            //    Console.Write(i);
            //}

            //2
            //for (int i = 2; i <= 20; i++,i++)
            //{
            //    Console.Write(i);
            //}

            //3
            //int length = Convert.ToInt32(Console.ReadLine());
            //for (int i = 1; i < length; i++, i++)
            //{
            //    Console.Write(i + " ");
            //}

            //4
            //int i = Convert.ToInt32(Console.ReadLine());
            //int Som = i;
            //while (i > 1)
            //{
            //    i--;
            //    Som = Som * i;                
            //}
            //Console.WriteLine(Som);

            ////5
            //int length = Convert.ToInt32(Console.ReadLine());
            //for (int i = 1; i <= length; i++)
            //{
            //    int t = i * i;
            //    Console.WriteLine(t);
            //}

            //6

            do
            {
                int a = Convert.ToInt32(Console.ReadLine());
                int b = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine(a*b);
                Console.WriteLine("Wilt u doorgaan (y/n)");
            } while (Console.ReadLine()=="y");
        }
    }
}
