﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Raadspel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int RandomGetal;
        private int Count = 0;
        public MainWindow()
        {
            InitializeComponent();
            Datumentijd.Text = DateTime.Now.ToString("dddd dd MMMM yyy HH':'mm':'ss");
            Random rnd = new Random();
            RandomGetal = rnd.Next(1, 100);
        }
        private void Nieuw(object sender, RoutedEventArgs e)
        {
            Random rnd = new Random();
            RandomGetal = rnd.Next(1, 100);
            SchattingGetal.Text = "";
            HogerLager.Text = "";
            AantalGeraden.Text = "";
        }
        private void BepaalWaarde(object sender, RoutedEventArgs e)
        {
            int SchattingGetalinInt = Convert.ToInt32(SchattingGetal.Text);
            Count++;
            if (SchattingGetalinInt > RandomGetal)
            {
                HogerLager.Text = "Raad Lager";
            }
            else if (SchattingGetalinInt < RandomGetal)
            { 
                HogerLager.Text = "Raad Hoger";
            }
            else 
            { 
                HogerLager.Text = "Proficiat! U hebt het Getal geraden!";
                AantalGeraden.Text = "Aantal Keren Geraden:" + Convert.ToString(Count);
            }
        }
    }
}
