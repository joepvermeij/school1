﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelectieOefeningenKL
{
    class Program
    {
        static void Main(string[] args)
        {
            // oefening 1
            // Twee gegeven getallen -> controleer welk het grootst is
            Console.WriteLine("geef getal");
            string inputTekst = Console.ReadLine();
            int getal = int.Parse(inputTekst);

            Console.WriteLine("geef 2de getal");
            string inputTekst2 = Console.ReadLine();
            int getal2 = int.Parse(inputTekst2);

            if (getal > getal2)
            {
                Console.WriteLine("getal 1 is het grootst");
            }
            else
            {
                Console.WriteLine("getal 2 is het grootst");
            }
            // Alternatief voor ctrl f5 is Console.ReadKey();

            Console.ReadKey();
        }
    }
}
