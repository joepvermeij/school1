﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ListsDemoWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }

        private void VoegToeButton_Click(object sender, RoutedEventArgs e)
        {
            if (MijnComboBox.SelectedIndex !=-1)
            {
                ComboBoxItem geselecteerdItem = (ComboBoxItem) MijnComboBox.SelectedItem;
                string geslacht = geselecteerdItem.Content.ToString();
                string naam = NaamTextBox.Text;
                ListBoxItem niewListBoxItem = new ListBoxItem();
                niewListBoxItem.Content = naam+" "+ geslacht;
                MijnListBox.Items.Add(niewListBoxItem);

            }
            
        }

        private void VerwijderButton_Click(object sender, RoutedEventArgs e)
        {
            MijnListBox.Items.Remove(MijnListBox.SelectedItem);
        }

        private void MijnListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (MijnListBox.SelectedItem != null)
            {
                ListBoxItem geselecteerdItem = (ListBoxItem)MijnListBox.SelectedItem;
                NaamTextBox.Text = geselecteerdItem.Content.ToString();
            }
        }
    }
}
