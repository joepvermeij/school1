﻿using System;

namespace ArrayDemoKL
{
    class Program
    {
        static void Main(string[] args)
        {
            #region startDemo
            //// declaratie
            //int[] reeksVanGetallen;
            //// initialisatie van grootte
            //reeksVanGetallen = new int[2];
            //string[] namen = new string[3] { "Kristof", "Tristan", "Mberk" };
            //Console.WriteLine(namen[0]);
            //namen[0] = "Jitske";
            //Console.WriteLine(namen[0]);
            //double kommagetal = 2.5;
            //double[] kommagetallen = new double[] { 2.5 , 2.6 , 2.7, 2.8, 2.9 };
            //int[] getallen = new int[] { 1, 2, 3 , 4 ,5 ,6 ,7  };
            //Console.WriteLine(getallen.Length);
            //for (int i = 0; i < getallen.Length; i++)
            //{
            //    Console.WriteLine(getallen[i]);
            //}
            #endregion
            int cijfer = 4;
            Console.WriteLine($"cijfer voor functie {cijfer}");
            DoStuff(ref cijfer);
            Console.WriteLine($"cijfer na functie {cijfer}");
            int[] cijfers = new int[4];
            cijfers[0] = 4;
            Console.WriteLine($"cijfer op index 0 voor functie {cijfers[0]}");
            DoStuff(cijfers);
            Console.WriteLine($"cijfer op index 0 na functie {cijfers[0]}");
            Console.ReadKey();
        }
        private static void DoStuff(ref int getal)
        {
            getal++;
            Console.WriteLine(getal);
        }
        private static void DoStuff(int[] getallen)
        {
            getallen[0]++;
            Console.WriteLine(getallen[0]);
        }
    }
}
