﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Toepassing32
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

        }
        private void btnSorteren(object sender, RoutedEventArgs e)
        {
            SimpleListBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));
            MultipleListBox.Items.SortDescriptions.Add(new System.ComponentModel.SortDescription("Content", System.ComponentModel.ListSortDirection.Ascending));
        }

        private void VerwijderItem(object sender, RoutedEventArgs e)
        {
            if (SimpleListBox.SelectedIndex != -1)
            {
                SimpleListBox.Items.Remove(SimpleListBox.SelectedItem);
            }
            if (MultipleListBox.SelectedIndex != -1)
            {
                List<ListBoxItem> selectedItems = new List<ListBoxItem>();
                foreach (ListBoxItem item in MultipleListBox.SelectedItems)
                {
                    selectedItems.Add(item);
                }
                foreach (ListBoxItem item in selectedItems)
                {
                    MultipleListBox.Items.Remove(item);
                }
            }
        }

        private void WisListBox(object sender, RoutedEventArgs e)
        {
            for (int i = SimpleListBox.Items.Count-1; i >=0 ; i--)
            {
                SimpleListBox.Items.RemoveAt(i);
            }
            for (int i = MultipleListBox.Items.Count - 1; i >= 0; i--)
            {
                MultipleListBox.Items.RemoveAt(i);
            }
        }

        private void Vervangen_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem item = new ListBoxItem();
            item.Content = VervangenTextBox.Text;
            if (SimpleListBox.SelectedIndex != -1 && VervangenTextBox.Text !=null)
            {
                int selectedIndex = SimpleListBox.SelectedIndex;
                SimpleListBox.Items.RemoveAt(selectedIndex);
                SimpleListBox.Items.Insert(selectedIndex, item);
            }
            if (MultipleListBox.SelectedIndex != -1 && VervangenTextBox.Text != null)
            {
                List<int> selectedItemsIndexes = new List<int>();
                foreach (ListBoxItem selectedItem in MultipleListBox.SelectedItems)
                {
                    selectedItemsIndexes.Add(MultipleListBox.Items.IndexOf(selectedItem));
                }
                foreach (int index in selectedItemsIndexes)
                {
                    ListBoxItem item2 = new ListBoxItem();
                    item2.Content = VervangenTextBox.Text;
                    MultipleListBox.Items.RemoveAt(index);
                    MultipleListBox.Items.Insert(index, item2);
                }
            }
        }

        private void Toevoegen_Click(object sender, RoutedEventArgs e)
        {
            ListBoxItem item = new ListBoxItem();
            item.Content = ToevoegenTextBox.Text;
            SimpleListBox.Items.Add(item);
            ListBoxItem item2 = new ListBoxItem();
            item2.Content = ToevoegenTextBox.Text;
            MultipleListBox.Items.Add(item2);
        }

        private void Zoeken_Click(object sender, RoutedEventArgs e)
        {
            string zoekenText = ZoekenTextBox.Text;
            if (komtOvereen(zoekenText))
            {
                ZoekenTextBlock.Text = "Item niet gevonden";
            }
        }
        private bool komtOvereen(string gezochteText)
        {
            ListBoxItem gezochteTextItem = new ListBoxItem();
            gezochteTextItem.Content = gezochteText;
            for (int i = 0; i < SimpleListBox.Items.Count; i++)
            {
                if (gezochteTextItem.ToString() == SimpleListBox.Items[i].ToString())
                {
                    ZoekenTextBlock.Text = $"Het woord {gezochteText} bevindt zich op plaats {i++}";
                    return false;
                }
            }
            return true;
        }

        private void MultipleListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int aantalSelectedItems = MultipleListBox.SelectedItems.Count;
            AantalItemsTextBox.Text = $"Er zijn {aantalSelectedItems} item(s) geselecteerd.";
        }
    }
}
