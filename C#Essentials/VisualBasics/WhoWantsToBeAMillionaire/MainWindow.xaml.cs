﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WhoWantsToBeAMillionaire
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string[,] vragen = new string[,]
            {
                { "100", "In the UK, the abbreviation NHS stands for National what Service?", "Humanity", "Health", "Honour", "Household", "2" },
                { "200", "Which Disney character famously leaves a glass slipper behind at a royal ball?", "Pocahontas", "Sleeping Beauty", "Cinderella", "Elsa", "3" },
                { "300", "What name is given to the revolving belt machinery in an airport that delivers checked luggage from the plane to baggage reclaim?", "Hangar", "Terminal", "Concourse", "Carousel", "4" },
                { "500", "Which of these brands was chiefly associated with the manufacture of household locks?" , "Phillips", "Flymo", "Chubb", "Ronseal", "3" },
                { "1000", "The hammer and sickle is one of the most recognisable symbols of which political ideology?", "Republicanism", "Communism", "Conservatism", "Liberalism", "2" },
                { "2000", "Which toys have been marketed with the phrase 'robots in disguise'?", "Bratz Dolls", "Sylvanian Families", "Hatchimals", "Transformers" , "4" },
                { "4000", "What does the word loquacious mean?", "Angry", "Chatty", "Beautiful", "Shy", "2" },
                { "8000", "Obstetrics is a branch of medicine particularly concerned with what?", "Childbirth", "Broken bones", "Heart conditions", "Old age", "1" },
                { "16000", "In Doctor Who, what was the signature look of the fourth Doctor, as portrayed by Tom Baker?", "Bow-tie, braces and tweed jacket", "Wide-brimmed hat and extra long scarf", "Pinstripe suit and trainers", "Cape, velvet jacket and frilly shirt", "2" },
                { "32000", "Which of these religious observances lasts for the shortest period of time during the calendar year?", "Ramadan", "Diwali", "Lent", "Hanukkah", "2" },
                { "64000", "At the closest point, which island group is only 50 miles south-east of the coast of Florida?", "Bahamas", "US Virgin Islands", "Turks and Caicos Islands", "Bermuda", "1"},
                { "125000", "Construction of which of these famous landmarks was completed first?" , "Empire State Building", "Royal Albert Hall", "Eiffel Tower", "Big Ben Clock Tower", "4" },
                { "250000", "Which of these cetaceans is classified as a 'toothed whale'?", "Gray whale", "Minke whale", "Sperm whale", "Humpback whale", "3" },
                { "500000", "Who is the only British politician to have held all four 'Great Offices of State' at some point during their career?" , "David Lloyd George", "Harold Wilson", "James Callaghan", "John Major", "3" },
                { "1 million", "In 1718, which pirate died in battle off the coast of what is now North Carolina?" , "Calico Jack", "Blackbeard", "Bartholomew Roberts", "Captain Kidd", "2" }
            };
        private DispatcherTimer timer;
        int vraagNummer = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromMilliseconds(900);
            timer.Tick += Timer_Tick;
            timer.Start();
            VraagTextBox.Text = vragen[vraagNummer, 1];
            int radioButtonIndex = 2;
            foreach (RadioButton rb in StackPanelAntwoorden.Children)
            {
                rb.Content = vragen[vraagNummer, radioButtonIndex];
                radioButtonIndex++;
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            if (TijdsComboBox.SelectedIndex == 1)
            {
                TijdsTextBlock.Text = DateTime.Now.ToShortDateString();
            }
            else if(TijdsComboBox.SelectedIndex == 2)
            {
                TijdsTextBlock.Text = DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToLongTimeString();
            }
            else
            {
                TijdsTextBlock.Text = DateTime.Now.ToLongTimeString();
            }
        }

        private void FinalAnswer(object sender, RoutedEventArgs e)
        {
            foreach (CheckBox cb in BonusCheckBox.Children)
            {
                if (cb.IsChecked != true)
                {
                    cb.IsEnabled = true;
                }
            }
            int answerNumber = 0;
            int correctAnswer = 0;
            foreach (RadioButton rb in StackPanelAntwoorden.Children)
            {
                answerNumber++;
                if (rb.IsChecked == true)
                {
                    correctAnswer = answerNumber;
                }
            }
            if (vraagNummer == 14 && correctAnswer.ToString() == vragen[14, 6])
            {
                VraagTextBox.Text = DollarTekenBouwer("Proficiat je bent een miljonair!");
                GeldTextBlock.Text = "1,000,000  gewonnen";
                MainDockPanel.Background = Brushes.Green;
                DisableEverything();
            }
            else if (correctAnswer.ToString() == vragen[vraagNummer, 6])
            {
                vraagNummer++;
                VraagTextBox.Text = vragen[vraagNummer, 1];
                int radioButtonIndex = 2;
                foreach (RadioButton rb in StackPanelAntwoorden.Children)
                {
                    rb.Content = vragen[vraagNummer, radioButtonIndex];
                    radioButtonIndex++;
                }
                GeldTextBlock.Text = vragen[vraagNummer, 0];
            }
            else
            {
                VraagTextBox.Text = DollarTekenBouwer("Helaas, je bent al het geld kwijt");
                GeldTextBlock.Text = "FOUT ANTWOORD";
                MainDockPanel.Background = Brushes.Red;
                DisableEverything();
                MessageBox.Show("Je bent verloren.", "Verloren");
            }
        }
        private string DollarTekenBouwer(string tekstFiller)
        {
            int aantalLetters = tekstFiller.Length;
            StringBuilder dollarTekens = new StringBuilder();
            for (int i = 0; i < aantalLetters+4; i++)
            {
                dollarTekens.Append("$");
            }
            dollarTekens.Append("\n$ ");
            for (int i = 0; i < aantalLetters; i++)
            {
                dollarTekens.Append(" ");
            }
            dollarTekens.Append(" $");
            dollarTekens.Append("\n$ ");
            dollarTekens.Append(tekstFiller);
            dollarTekens.Append(" $");
            dollarTekens.Append("\n$ ");
            for (int i = 0; i < aantalLetters; i++)
            {
                dollarTekens.Append(" ");
            }
            dollarTekens.Append(" $\n");
            for (int i = 0; i < aantalLetters + 4; i++)
            {
                dollarTekens.Append("$");
            }
            return dollarTekens.ToString();
        }
        private void DisableEverything()
        {
            BonusCheckBox1.IsEnabled = false;
            BonusCheckBox2.IsEnabled = false;
            BonusCheckBox3.IsEnabled = false;
            Antwoord1RadioButton.IsEnabled = false;
            Antwoord2RadioButton.IsEnabled = false;
            Antwoord3RadioButton.IsEnabled = false;
            Antwoord4RadioButton.IsEnabled = false;
            VraagTextBox.IsEnabled = false;
            BonusListBox.IsEnabled = false;
            TijdsComboBox.IsEnabled = false;
            FinalAnswerButton.IsEnabled = false;
        }
        private void BonusCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            BonusCheckBoxSelector();
        }
        private void BonusCheckBoxSelector()
        {
            BonusCheckBox1.IsEnabled = false;
            BonusCheckBox2.IsEnabled = false;
            BonusCheckBox3.IsEnabled = false;
            Random random = new Random();
            int randomHint1Nummer = Convert.ToInt32(vragen[vraagNummer, 6]);
            while (randomHint1Nummer == Convert.ToInt32(vragen[vraagNummer, 6]))
            {
                randomHint1Nummer = random.Next(1, 5);
            }
            int randomHint2Nummer = Convert.ToInt32(vragen[vraagNummer, 6]);
            while (randomHint2Nummer == Convert.ToInt32(vragen[vraagNummer, 6]) || randomHint2Nummer == randomHint1Nummer)
            {
                randomHint2Nummer = random.Next(1, 5);
            }
            ListBoxItem hintListBoxItem = new ListBoxItem();
            hintListBoxItem.Content = $"Bonus voor {vragen[vraagNummer, 0]}: {vragen[vraagNummer, randomHint1Nummer + 1]} {vragen[vraagNummer, randomHint2Nummer + 1]}";
            BonusListBox.Items.Add(hintListBoxItem);
        }
    }
}
