﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Toepassing33Inschrijving
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Dictionary<string, double> graduaatInformatica; 
        public MainWindow()
        {
            InitializeComponent();
            VulDictionary();
            VulComboBox();
        }
        private void VulDictionary()
        {
            graduaatInformatica = new Dictionary<string, double>();
            graduaatInformatica.Add("Programmeren", 920.80);
            graduaatInformatica.Add("Netwerkbeheer", 920.80);
            graduaatInformatica.Add("Internet of Things", 520.80);
            graduaatInformatica.Add("Digitale Vormgever", 720.80);
            graduaatInformatica.Add("Drone opleiding", 520.80);
        }
        private void VulComboBox()
        {
            foreach (string key in graduaatInformatica.Keys)
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = key;
                graduaatInformaticaComboBox.Items.Add(item);
            }
        }

        private void Berekenen_Click(object sender, RoutedEventArgs e)
        {
            double basisBedrag;
            ComboBoxItem geselecteerdItem = (ComboBoxItem)graduaatInformaticaComboBox.SelectedItem;
            basisBedrag = graduaatInformatica[geselecteerdItem.Content.ToString()];
            double teBetalen = basisBedrag;
            if (LagerSecundairRadioButton.IsChecked == true)
            {
                teBetalen = 0.7 * teBetalen;
            }
            else if (HogersecundairRadioButton.IsChecked == true)
            {
                teBetalen = 0.8 * teBetalen;
            }
            else if (MasterRadioButton.IsChecked == true)
            {
                teBetalen = 1.1 * teBetalen;
            }
            if (WerkzoekenCheckBox.IsChecked == true)
            {
                teBetalen = 0.5 * teBetalen;
            }
            ResultaatTextBox.Text = $"INSCHRIJVINGSBEDRAG VOOR: {NaamTextBox.Text} \n\nBasisbedrag: € {basisBedrag} \nTe betalen: € {teBetalen}";
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            NaamTextBox.Text = "";
            ResultaatTextBox.Text = "";
            LagerSecundairRadioButton.IsChecked = true;
            WerkzoekenCheckBox.IsChecked = false;
            graduaatInformaticaComboBox.SelectedIndex = -1;
            NaamTextBox.Focus();
        }
    }
}
