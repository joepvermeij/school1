﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace frmReiskost2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Berekenen_click(object sender, RoutedEventArgs e)
        {
            float totvluchtprijs;
            float totverblijfplaats;
            float totreisprijs;
            float korting;
            float tebetalen;
            float basisvluchtprijs;
            float Prijsperdagingetal;
            float aantalpersonen;
            float aantaldagen;
            

            aantalpersonen = float.Parse(aantal_personen.Text);
            aantaldagen = float.Parse(aantal_dagen.Text);
            if (Vluchtklasse.Text == "1")
            {
                basisvluchtprijs = float.Parse(Basisvlucht.Text) * 1.3f;
            }
            else if (Vluchtklasse.Text == "3")
            {
                basisvluchtprijs = float.Parse(Basisvlucht.Text) - (.2f * float.Parse(Basisvlucht.Text));
            }
            else
            {
                basisvluchtprijs = float.Parse(Basisvlucht.Text);
            }
            totvluchtprijs = basisvluchtprijs * aantalpersonen;          
            if (aantal_personen.Text == "1" || aantal_personen.Text == "2")
            {
                totverblijfplaats = aantalpersonen * aantaldagen * float.Parse(Prijsperdag.Text);
            }
            else
            {
                totverblijfplaats = (aantaldagen * float.Parse(Prijsperdag.Text) * 2.5f) + ((aantalpersonen - 3) * aantaldagen * float.Parse(Prijsperdag.Text) * .3f);
            }
            totreisprijs = totverblijfplaats + totvluchtprijs;
            korting = totreisprijs * (float.Parse(kortingspercentage.Text) / 100);
            tebetalen = totreisprijs - korting;

            

            resultaat.Text = $"REISKOST VOLGENS BESTELLING NAAR {Bestemming.Text} \n \nTotale Vluchtprijs: {totvluchtprijs} \nTotal Verblijfsprijs: {totverblijfplaats}\n" +
                $"Totale Rijsprijs: {totreisprijs} \n" +
                $"Totale Korting: {korting} \n \n" +
                $"Te betalen: {tebetalen}";
        }

        private void Wissen_Click(object sender, RoutedEventArgs e)
        {
            kortingspercentage.Text = "";
            aantal_personen.Text = "";
            aantal_dagen.Text = "";
            Prijsperdag.Text = "";
            Vluchtklasse.Text = "";
            Basisvlucht.Text = "";
            Bestemming.Text = "";
            resultaat.Text = "";
        }

        private void Afsluiten(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
