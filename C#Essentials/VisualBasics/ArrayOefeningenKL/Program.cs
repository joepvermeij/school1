﻿using System;
using System.Collections.Generic;

namespace ArrayOefeningenKL
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> lijst= new List<int> { 1, 2, 3, 4, 5, 6 };
            List<int> lijst2 = new List<int> { 6, 7, 8, 9, 10, 11 };
            MergeTweeLijsten(lijst, lijst2);
        }
        // oef 2
        private static bool IsLangerDanTien(string[] Array)
        {
            if (Array.Length > 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //oef 3
        private static int[] TienRandomGetallen()
        {
            int[] tienWillekeurigeGetallen = new int[10];
            for (int i = 0; i < tienWillekeurigeGetallen.Length; i++)
            {
                Random randomGetal = new Random();
                int getal = randomGetal.Next(1, 11);
                tienWillekeurigeGetallen[i] = getal;
                Console.WriteLine(tienWillekeurigeGetallen[i]);
            }
            return tienWillekeurigeGetallen;
        }

        //oef 9
        private static int[] Insert(int[] array, int positie, int waarde)
        {
            int[] newArray = new int[array.Length + 1];
            for (int i = 0; i < positie; i++)
            {
                newArray[i] = array[i];
            }
            newArray[positie] = waarde;
            for (int i = array.Length; i > positie; i--)
            {
                newArray[i] = array[i - 1];
            }
            return newArray;
        }

        //oef 11
        private static int TweedeGrootsteElement(int[] reeks)
        {
            int aantalGrotereWaardes = 0;
            int waarde;
            int tweedeGrootsteGetal = 0;
            for (int i = 0; i < reeks.Length; i++)
            {
                waarde = reeks[i];
                for (int a = 0; a < reeks.Length; a++)
                {
                    if (waarde < reeks[a])
                    {
                        aantalGrotereWaardes++;
                    }
                }
                if (aantalGrotereWaardes == 1)
                {
                    tweedeGrootsteGetal = waarde;
                }
                else
                {
                    aantalGrotereWaardes = 0;
                }
            }
            return tweedeGrootsteGetal;
        }
        //lists
        //oef 6
        private static List<int> WillekeurigeSubset(List<int> list, int grootte)
        {
            List<int> subset = new List<int>();
            for (int i = 0; i < grootte; i++)
            {
                Random rand = new Random();
                int randomGetal = rand.Next(list.Count);
                int randomElement = list[randomGetal];
                subset.Add(randomElement);
                Console.WriteLine(subset[i]);
            }
            return subset;
        }
        //oef 7
        private static bool KomenElementenOvereen(List<int> a, List<int> b)
        {
            for (int i = 0; i < a.Count; i++)
            {
                for (int j = 0; j < b.Count; j++)
                {
                    if (a[i] == b[j])
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        //oef 8
        private static List<int> MergeTweeLijsten(List<int> a, List<int> b)
        {
            List<int> volledigelijst;
            volledigelijst = a;
            for (int i = 0; i < b.Count; i++)
            {
                volledigelijst.Add(b[i]);
            }
            for (int i = 0; i < volledigelijst.Count; i++)
            {
                Console.WriteLine(volledigelijst[i]);
            }
            return volledigelijst;
        }
    }
}
