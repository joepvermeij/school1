﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.VisualBasic;

namespace Raadspelletje
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Start_Click(object sender, RoutedEventArgs e)
        {
            Random random = new Random();
            int willekeurigGetal = random.Next(100);
            RaadGetal(willekeurigGetal);
        }
        private void RaadGetal(int teRadenGetal)
        {
            bool isConverteerbaar;
            int guess;
            int count = 0;
            do
            {
                count++;
                do
                {
                    isConverteerbaar = int.TryParse(Interaction.InputBox("Geef een getal tussen 1 en 100"), out guess);
                } while (!isConverteerbaar);
                if (guess == teRadenGetal)
                {
                    MessageBox.Show($"U heeft het getal geraden in {count} beurten");
                }
                else if (guess > teRadenGetal)
                {
                    HogerLagerImage.Source = new BitmapImage(new Uri(@"\Image\emoji_thumbs-down_full.jpg", UriKind.Relative));
                    MessageBox.Show("Raad lager");
                }
                else
                {
                    HogerLagerImage.Source = new BitmapImage(new Uri(@"\Image\image.jpg", UriKind.Relative));
                    MessageBox.Show("Raad hoger");
                }
            } while (guess != teRadenGetal);
        }
    }
}
