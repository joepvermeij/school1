﻿using System;

namespace Foutafhandeling_oefeningen
{
    class Program
    {
        static void Main(string[] args)
        {
            string a = "5";
            Console.WriteLine();
        }
        private static int DoubleInput(string input)
        {
            int getal = 0;
            try
            {
                getal = int.Parse(input);​
            }
            catch (FormatException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }
            catch(ArgumentNullException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch(OverflowException e)
            {
                Console.WriteLine(e.Message);
            }
            return getal;
        }
        private static int TryDoubleInputSafe(string input)
        {
            int getal = 0;
            if (string.IsNullOrEmpty(input))
            {
                Console.WriteLine("De input was leeg");
                string nieuweInput = Console.ReadLine();
                return TryDoubleInputSafe(nieuweInput);
            }
            else if(!int.TryParse(input, out getal))
            {
                Console.WriteLine("De input was ongeldig");
                string nieuweInput = Console.ReadLine();
                return TryDoubleInputSafe(nieuweInput);
            }
            else
            {
                getal = int.Parse(input);
                return getal;
            }
        }
        private static string veranderSpatiesEnLeestekensNaarUnderscore(string tekst)
        {
            string​​result​ = tekst.​Replace​(​" "​, ​"_"​);


        }
    }
}
