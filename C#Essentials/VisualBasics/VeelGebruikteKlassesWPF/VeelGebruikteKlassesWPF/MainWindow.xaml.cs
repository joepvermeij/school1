﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace VeelGebruikteKlassesWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            // declaren van dispatcher timer (vergeer using statement niet)
            DispatcherTimer timer;
            // constructor gebruiken voor een nieuwe instantie aan te maken
            timer = new DispatcherTimer();
            
            // Timespan instellen in interval
            timer.Interval = new TimeSpan(0,0,1);

            // gebeurtenis/actie instellen wanneer interval verloopt
            timer.Tick += TimerTick;

            // start de timer
            timer.Start();

        }

        private void TimerTick(object sender, EventArgs e)
        {
            // verwijder geforceerde exception
            TimerText.Text = DateTime.Now.ToShortTimeString();
            RandomBackground();
        }

        private void RandomBackground()
        {
            Random random = new Random();

            double randomGetal = random.NextDouble();
            int randomGeheelGetal = random.Next(0,2);

            if (randomGeheelGetal == 0)
            {
                TimerText.Background = Brushes.Gray;

            }
            else
            {
                TimerText.Background = Brushes.LightGray;
            }

        }
    }
}
