﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace _2DArrayDemoWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            int[] reeks = new int[4];
            int[,] matrix;
            string[,] woordenveld;
            StringBuilder[,] stringBuilderRaser;

            // 4 rijen en 3 kolommen
            matrix = new int[4, 3];
            matrix = new int[,]
            {
                {0,0 },
                {1,2 },
                {1001,2 },
                {1,-5 }
            };
            StringBuilder output = new StringBuilder();

            output.AppendLine(matrix[0, 0] + "");
            output.AppendLine(matrix[1, 1].ToString());
            output.AppendLine($"De matrix heef {matrix.GetLength(0)} rijen");
            output.AppendLine($"De matrix heef {matrix.Length} elementen");

            output.AppendLine();

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    output.Append(matrix[i,j] + " ");
                }
                output.AppendLine();

            }

            int grootsteWaarde=int.MinValue;
            int kleinsteWaarde =int.MaxValue;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (matrix[i,j]>grootsteWaarde)
                    {
                        grootsteWaarde = matrix[i, j];

                    }
                    if (matrix[i, j] < kleinsteWaarde)
                    {
                        kleinsteWaarde = matrix[i, j];
                    }
                }
            }
            output.AppendLine(kleinsteWaarde.ToString());
            output.AppendLine(grootsteWaarde.ToString());

            int[] getallen = new int[] { 0,1,2,3,4,5,6};
            List<int> lijst = new List<int>(getallen);

            lijst.Add(7);
            lijst.IndexOf(5);
            lijst.RemoveAt(2);
            lijst.Clear();

            // werkt niet
            // getallen.Indexof();

            // Verschil: static vs non static
            //static methods: uitgevoerd op de klasse
            int absoluteWaarde = Math.Abs(-2);
            // non static methods: uitgevoerd op object/instantie van de klasse
            Random random = new Random();
            int willekeurigeGetal = random.Next(10);

            //  HULPFUNCTIES ARRAY:
            int indexVanGetal = Array.IndexOf(getallen, 5);
            output.AppendLine($"{indexVanGetal}");
            TextBoxOutput.Text = output.ToString();
            
        }
    }
}
