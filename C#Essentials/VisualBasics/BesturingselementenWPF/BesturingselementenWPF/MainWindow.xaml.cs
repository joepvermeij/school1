﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BesturingselementenWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            CheckBoxStackPanel.Background = Brushes.Azure;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button myButton;
            CheckBox myCheckBox;
            string myString;
            
            if(MijnCheckBox.IsChecked == true)
            {
                OutputLabel.Content = "Het is een man";
            }
            else
            {
                OutputLabel.Content = "Het is een vrouw";
            }
        }

        private void MijnCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            OutputLabel.Content = "Je klikte op de checkbox";
        }

        private void Collapsed_Click(object sender, RoutedEventArgs e)
        {
            CheckBoxStackPanel.Visibility = Visibility.Collapsed;
        }

        private void Visible_Click(object sender, RoutedEventArgs e)
        {
            CheckBoxStackPanel.Visibility = Visibility.Visible;
        }

        private void Hidden_Click(object sender, RoutedEventArgs e)
        {
            CheckBoxStackPanel.Visibility = Visibility.Hidden;
        }
    }
}
