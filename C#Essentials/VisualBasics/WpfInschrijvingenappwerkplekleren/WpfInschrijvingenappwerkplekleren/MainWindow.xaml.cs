﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfInschrijvingenappwerkplekleren
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            String RuimteText = Ruimte.Text;
            String NaamText = Naam.Text;
            

            if(RuimteText == "1" && count1 < 10)
            {
                count1++;
                Ruimte1.Text = Ruimte1.Text + Convert.ToString(count1) + ". " + NaamText + "\n";
                MessageBox.Show($"Welkom in Ruimte {RuimteText}");
            }
            else if(RuimteText == "2" && count2 < 10)
            {
                count2++;
                Ruimte2.Text = Ruimte2.Text + Convert.ToString(count2) + ". " + NaamText + "\n";
                MessageBox.Show($"Welkom in Ruimte {RuimteText}");
            }
            else if(RuimteText == "3" && count3 < 10)
            {
                count3++;
                Ruimte3.Text = Ruimte3.Text + Convert.ToString(count3) + ". " + NaamText + "\n";
                MessageBox.Show($"Welkom in Ruimte {RuimteText}");
            }
            else
            {
                MessageBox.Show($"Het Maximale aantal afwezigheden voor ruimte {RuimteText} is bereikt");
            }
        }

        private void AfsluitenClick(object sender, RoutedEventArgs e)
        {

            MessageBoxResult antw = MessageBox.Show("Bent u zeker dat u de applicatie wil aflsuiten","Afsluiten", MessageBoxButton.YesNo);
            if(antw == MessageBoxResult.Yes)
            {
                this.Close();
            }          
        }
    }
}
